package com.example.miexamen02.datos;

import android.content.Context;
import android.widget.ProgressBar;

import java.util.List;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public interface DataBaseDataSource
{
    void fetchItems() throws BaseDataItemsException;
    void giveItems(List<Persona> listaPersonas);
}