package com.example.miexamen02.datos;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.miexamen02.presentacion.MainActivityView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AsyncTaskRest extends AsyncTask<String, Integer, Void>
{
    private List<Persona> listaPersonas;
    private DataBaseSourceREST dataBaseSourceREST;
    private MainActivityView mainActivityView;

    private static int MAX_PROGRESO = 100;

    public AsyncTaskRest(DataBaseSourceREST dataBaseSourceREST, MainActivityView mainActivityView)
    {
        this.dataBaseSourceREST = dataBaseSourceREST;
        this.mainActivityView = mainActivityView;
        this.listaPersonas = new ArrayList<>();
    }

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        mainActivityView.getProgressBar().setMessage("Ejecutando tarea asincrónica...");
        mainActivityView.getProgressBar().setTitle("Progreso");
        mainActivityView.getProgressBar().setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mainActivityView.getProgressBar().setCancelable(false);
        mainActivityView.getProgressBar().setMax(MAX_PROGRESO);
        mainActivityView.getProgressBar().show();
    }

    @Override
    protected Void doInBackground(String... urls)
    {
        String json = obtenerJson(urls[0]);

        if(json != null)
            procesarJson(json);

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values)
    {
        super.onProgressUpdate(values);

        mainActivityView.getProgressBar().setProgress(values[0]);
    }

    // Cuando se termina de ejecutar la tarea
    @Override
    protected void onPostExecute(Void result)
    {
        // Algo tengo que hacer con la lista de personas, ya que ya estan pobladas. El profe menciona algo sobre devolverlo a la actividad, pero como? Algo que tenga que ver con context
        dataBaseSourceREST.giveItems(listaPersonas);
        mainActivityView.getProgressBar().dismiss();
    }

    private String obtenerJson(String urlParam)
    {
        InputStream inputStream = null;
        HttpURLConnection conexion = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;

        try
        {
            URL url = new URL(urlParam);
            conexion = (HttpURLConnection) url.openConnection();
            conexion.connect();

            // Si no da ok, hay error
            if (conexion.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }

            // Para el progreso. Se medira acorde se vaya leyendo las lineas
            int tamannoTotal = conexion.getContentLength();
            int acumuladorTamanno = 0;

            // Setteo para leer el string
            inputStream = conexion.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder strBuilder = new StringBuilder();
            String linea;

            while ((linea = bufferedReader.readLine()) != null)
            {
                // Empiezo a acumular el tamanno de cada linea para mostrar progreso
                acumuladorTamanno += linea.length();
                strBuilder.append(linea);
                publishProgress((acumuladorTamanno * MAX_PROGRESO) / tamannoTotal);
            }

            return strBuilder.toString();
        }
        catch (Exception e)
        {
            return null;
        }
        finally
        {
            try
            {
                if (inputStream != null)
                    inputStream.close();

                if(inputStreamReader != null)
                    inputStreamReader.close();

                if(bufferedReader != null)
                    bufferedReader.close();
            }
            catch (IOException ignored)
            {
            }

            if (conexion != null)
                conexion.disconnect();
        }
    }

    private void procesarJson(String json)
    {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        GsonListaPersonas lista = gson.fromJson(json, GsonListaPersonas.class);
        listaPersonas = lista.personas;
    }

    private static class GsonListaPersonas
    {
        @SerializedName("miPersonas")
        private List<Persona> personas;
    }
}
