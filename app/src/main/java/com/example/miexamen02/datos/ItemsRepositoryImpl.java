package com.example.miexamen02.datos;

import android.widget.ProgressBar;

import com.example.miexamen02.negocio.GetListItemsInteractor;
import com.example.miexamen02.presentacion.MainActivityView;

import java.util.List;

// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes
public class ItemsRepositoryImpl implements ItemsRepository
{
    private DataBaseDataSource mDataBaseDataSource;
    private GetListItemsInteractor getListItemsInteractor;


    public ItemsRepositoryImpl(GetListItemsInteractor getListItemsInteractor, MainActivityView mainActivityView)
    {
        this.getListItemsInteractor = getListItemsInteractor;
        this.mDataBaseDataSource = new DataBaseSourceREST(this, mainActivityView);
    }

    @Override
    public void giveItems(List<Persona> listaPersonas)
    {
        getListItemsInteractor.giveItems(listaPersonas);
    }

    @Override
    public void fetchItems() throws CantRetrieveItemsException
    {
        // Se realizan las llamadas a las fuentes de datos de donde se obtienen los datos
        // Ejemplo: base de datos local, archivos locales, archivos en la red, bases de datos en la red
        try
        {
            mDataBaseDataSource.fetchItems();
        }
        catch (BaseDataItemsException e)
        {
            throw new CantRetrieveItemsException(e.getMessage());
        }
    }
}
