package com.example.miexamen02.datos;

import android.widget.ProgressBar;

import com.example.miexamen02.presentacion.MainActivityView;

import java.util.List;

public class DataBaseSourceREST implements DataBaseDataSource
{
    private static final String REST_URL = "https://bitbucket.org/lyonv/ci0161_i2020_exii/raw/4250ce77dc7fdb5f5c1324d8c8d5bf5ff3959740/Personas6.json";

    private AsyncTaskRest asyncTaskRest;

    private ItemsRepository itemsRepository;

    public DataBaseSourceREST(ItemsRepository itemsRepository, MainActivityView mainActivityView)
    {
        this.itemsRepository = itemsRepository;
        asyncTaskRest = new AsyncTaskRest(this, mainActivityView);
    }

    @Override
    public void fetchItems()
    {
        asyncTaskRest.execute(REST_URL);
    }

    @Override
    public void giveItems(List<Persona> listaPersonas)
    {
        itemsRepository.giveItems(listaPersonas);
    }
}
