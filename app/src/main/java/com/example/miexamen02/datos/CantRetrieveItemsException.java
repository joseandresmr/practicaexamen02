package com.example.miexamen02.datos;

public class CantRetrieveItemsException extends Exception
{
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}