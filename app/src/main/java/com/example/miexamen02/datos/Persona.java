package com.example.miexamen02.datos;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Persona implements Parcelable
{

    @NonNull
    private String identificacion;

    private String nombre;

    public static final String key = "Persona";

    public Persona(String identificacion, String nombre)
    {
        this.identificacion = identificacion;
        this.nombre = nombre;
    }

    public Persona(Parcel in)
    {
        this.identificacion = in.readString();
        this.nombre = in.readString();
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(identificacion);
        dest.writeString(nombre);
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>()
    {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }
        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };
}