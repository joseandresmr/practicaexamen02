package com.example.miexamen02.negocio;

import android.widget.ProgressBar;

import com.example.miexamen02.datos.Persona;

import java.util.List;

public interface GetListItemsInteractor
{
    interface OnFinishedListener {
        void onFinished(List<Persona> items);
    }
    void giveItems(List<Persona> items);
    void fetchItems();
    boolean wasFetched();
}
