package com.example.miexamen02.presentacion;

import android.app.ProgressDialog;
import com.example.miexamen02.datos.Persona;

import java.util.List;

// Capa de presentacion (Vista)
// Implementado por MainActivity
public interface MainActivityView
{
    // Esconder el indicador de progreso de la UI
    void hideProgress();

    // Mostrar los items de la lista en la UI
    void setItems(List<Persona> items);

    // Mostrar mensaje en la UI
    void showMessage(String message);

    void irDetalles(int position);

    ProgressDialog getProgressBar();

}
