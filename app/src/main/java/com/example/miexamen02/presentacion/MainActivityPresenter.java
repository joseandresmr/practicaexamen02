package com.example.miexamen02.presentacion;

import com.example.miexamen02.datos.Persona;

import java.util.List;

public interface MainActivityPresenter
{
    // resumir
    void onResume();
    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(int position);
    // destruir
    void onDestroy();
}
