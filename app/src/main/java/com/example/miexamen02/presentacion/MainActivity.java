package com.example.miexamen02.presentacion;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.miexamen02.DetallesPersonaActivity;
import com.example.miexamen02.R;
import com.example.miexamen02.datos.Persona;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        // interface View (V) para implementar los metodos de UI
        MainActivityView,
        // interface para implementar el listener del metodo onItemClick de la lista
        AdapterView.OnItemClickListener
{
    private ListView mListView;
    private ProgressDialog mProgressDialog;
    private MainActivityPresenter mMainActivityPresenter;

    private List<Persona> listaPersonas;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listaPersonas = new ArrayList<>();

        mListView = findViewById(R.id.list);
        mListView.setOnItemClickListener(this);
        mProgressDialog = new ProgressDialog(this);
        // Llamada al Presenter

        mMainActivityPresenter = new MainActivityPresenterImpl(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onDestroy()
    {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    // Esconder el indicador de progreso de la UI
    @Override
    public void hideProgress() {
        mProgressDialog.hide();
        mListView.setVisibility(View.VISIBLE);
    }
    // Mostrar los items de la lista en la UI
    // Con la lista de items muestra la lista
    @Override
    public void setItems(List<Persona> items)
    {
        listaPersonas = items;
        List<String> nombres = new ArrayList<String>();

        for(Persona persona : items)
        {
            nombres.add(persona.getNombre());
        }

        mListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                nombres));
    }
    // Mostrar mensaje en la UI
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void irDetalles(int position)
    {
        if(position < listaPersonas.size())
        {
            Intent intent = new Intent(this, DetallesPersonaActivity.class);
            Persona persona = listaPersonas.get(position);
            intent.putExtra(Persona.key, persona);
            startActivity(intent);
        }
    }

    @Override
    public ProgressDialog getProgressBar()
    {
        return mProgressDialog;
    }

    // Evento al dar clic en la lista
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        mMainActivityPresenter.onItemClicked(position);
    }
}