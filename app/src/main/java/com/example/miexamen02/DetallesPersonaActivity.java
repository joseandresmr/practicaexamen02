package com.example.miexamen02;

import android.app.FragmentTransaction;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.miexamen02.datos.Persona;

public class DetallesPersonaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_persona);

        // Obtener objeto
        Persona persona = getIntent().getParcelableExtra(Persona.key);

        final PersonaFragmento tableTopFragment = PersonaFragmento.newInstance(persona);

        // Transaccion para inicializar y asignar el fragmento
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, tableTopFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }
}
